from distutils.core import setup
setup(name='rpi-dht-snmp',
      version='0.1',
      author='Mike Kelly',
      author_email='pioto@pioto.org',
      description='SNMP AgentX implementation to support Adafruit DHT temperature/humidity sensors on the Raspberry Pi.',
      license='MIT',
      url='https://gitlab.com/pioto/rpi-dht-snmp',
      scripts=['scripts/rpi-dht-snmp'],
      requires=['Adafruit_DHT', 'pysnmp', 'netsnmpagent'])
