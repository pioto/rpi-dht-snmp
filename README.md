# What is this?

This project intends to provide a simple SNMP "AgentX" implementation to
talk to sensors supported by Adafruit's [Python
DHT](https://github.com/adafruit/Adafruit_Python_DHT) library, namely
the DHT11, DHT22, and AM2302 temperature/humidity sensors.

(Note, it is only tested against the AM2302, but hopefully will work
with minimal configuration on the DHT11 or DHT22; patches welcome.)

There are two parts to this project:

* An AgentX script, which is intended to be running as a daemon; when
  appropriate SNMP requests come in, the agent responds with the
  available data. This is based upon the [netsnmpagent](https://github.com/pief/python-netsnmpagent)
  Python module.
* An SNMP
  [MIB](https://en.wikipedia.org/wiki/Management_information_base), to
  allow for easy integration with various bits of network monitoring
  software.

# Example

A brief example of the sort of output this agent generates:

```
$ snmptable -Ci -v 2c -c XXXX rpi.example.com RPI-DHT-MIB::rpiDhtSensorsTable
SNMP table: RPI-DHT-MIB::rpiDhtSensorsTable

 index rpiDhtSensorType rpiDhtSensorTemp rpiDhtSensorHumidity
     4           am2302           28.1 C               31.2 %
```
